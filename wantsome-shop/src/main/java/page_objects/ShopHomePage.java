package page_objects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class ShopHomePage {
    private final By CART_BTN = By.xpath("//a[text()='Cart']");
    private WebDriver driver;

    public ShopHomePage(WebDriver webDriver) {
        this.driver = webDriver;
    }

    public CartPage navigateToCart() {
        driver.findElement(CART_BTN).click();
        return new CartPage(driver);
    }
}
