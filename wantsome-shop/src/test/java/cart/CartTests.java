package cart;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import page_objects.CartPage;
import page_objects.ShopHomePage;
import utils.BaseTestClass;

public class CartTests extends BaseTestClass {

    @Test
    public void checkEmptyCart(){
        //click on Cart tab
        driver.findElement(By.xpath("//a[text()='Cart']")).click();

        //check Cart page title
        Assert.assertEquals("CART", driver.findElement(By.className("entry-title")).getText());

        //check Cart page message
        Assert.assertEquals("Your cart is currently empty.",driver.findElement(By.className("cart-empty")).getText());
    }

    @Test
    public void checkEmptyCartPageObject(){
        CartPage cartPage = new ShopHomePage(driver).navigateToCart();
        Assert.assertEquals("CART", cartPage.getTitle());
        Assert.assertEquals("Your cart is currently empty.", cartPage.getEmptyCartText());
    }

    @Test
    public void addToCart(){
        driver.get("https://testare-automata.practica.tech/shop/?product_cat=men-collection");
        Actions actions = new Actions(driver);
        actions.moveToElement(driver.findElement(By.cssSelector(".products-img a img")))
                .build()
                .perform();
        actions.moveToElement(driver.findElement(By.xpath("//figure//div[@class='products-hover-block']")))
                .build()
                .perform();
        driver.findElement(By.className("add_to_cart_button")).click();
    }
}
